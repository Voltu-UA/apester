import { AppController } from '../src/modules/app.controller';
import { AppService } from '../src/modules/app.service';

describe('AppController', () => {
  let appController: AppController;
  let appService: AppService;

  beforeEach(() => {
    appService = new AppService();
    appController = new AppController(appService);
  });

  describe('getAds', () => {
    it('should return an array of ads', async () => {
      const queryArgsMock = {
        lat: '50.486131241880024',
        long: '30.454430460540802',
        tag: ['travel', 'europe'],
      };

      const headersMock = {
        'user-agent':
          'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36',
      };

      const resultMock = [
        {
          id: '1',
          description: 'Test Add',
          imageUrl: 'test.img.com',
          targeting: {
            location: {
              lat: 50.450001,
              long: 30.523333,
              radius: 100000,
            },
            operatingSystems: ['MacOS', 'Linux', 'Windows'],
            browsers: ['Safari', 'Mozilla', 'Edge'],
            tags: ['travel', 'europe', 'ukraine'],
          },
        },
        {
          id: '2',
          description: 'Test Add 2',
          imageUrl: 'test.img2.com',
          targeting: {
            location: {
              lat: 50.450001,
              long: 30.523333,
              radius: 100000,
            },
            operatingSystems: ['MacOS', 'Linux', 'Windows'],
            browsers: ['Safari', 'Mozilla', 'Edge'],
            tags: ['travel', 'asia', 'israel'],
          },
        },
      ];

      jest
        .spyOn(appService, 'getAds')
        .mockImplementation(async () => resultMock);
      jest.spyOn(appController, 'getAds');

      const response = await appController.getAds(queryArgsMock, headersMock);
      expect(response).toBe(resultMock);
      expect(response).toHaveLength(2);
      expect(response[0]).toBeInstanceOf(Object);
      expect(appService.getAds).toHaveBeenCalled();
      expect(appService.getAds).toHaveBeenCalledTimes(1);
      expect(appController.getAds).toHaveBeenCalled();
      expect(appController.getAds).toHaveBeenCalledTimes(1);
      expect(appController.getAds).toHaveBeenCalledWith(
        queryArgsMock,
        headersMock,
      );
    });
  });
});
