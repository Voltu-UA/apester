import { constants } from 'fs';
import { writeFile, readFile, access } from 'fs/promises';

import {
  HttpStatus,
  Injectable,
  InternalServerErrorException,
  NotFoundException,
  ServiceUnavailableException,
} from '@nestjs/common';
import { isPointWithinRadius } from 'geolib';
import { UAParser } from 'ua-parser-js';
import { intersection, orderBy } from 'lodash';

import { CreateAdDto } from '../dto/create-ad.dto';
import { BROWSERS, OS } from '../constants';
import { PrioritizedAdsData } from '../interfaces';

@Injectable()
export class AppService {
  async saveAdd(ad: CreateAdDto): Promise<void> {
    try {
      /* 
        Check if data.json file exists and if not,
        handle this scenario in catch block
      */
      await access('src/db/data.json', constants.F_OK);

      /* 
        Read previously saved data from data.json file.
        Also handle scenario when data.json file is empty.
      */
      const savedAdsData = (await readFile('src/db/data.json', 'utf8')) || '[]';
      const parsedData = JSON.parse(savedAdsData);
      parsedData.push(ad);

      // Save new ad data from request to data.json file.
      await writeFile('src/db/data.json', JSON.stringify(parsedData));
    } catch (error) {
      if (error.code === 'ENOENT') {
        await writeFile('src/db/data.json', JSON.stringify([ad]));
      } else {
        /* 
          Logging here is just for development purposes,
          because we respond with empty body object as required in task.
        */
        console.error(
          `Error occured during ad creation: ${error?.message || error}`,
        );
        throw new InternalServerErrorException({});
      }
    }
  }

  async getAds(
    queryArgs: { [x: string]: any },
    headers: string,
  ): Promise<CreateAdDto[]> {
    try {
      const { lat: userLat, long: userLong, tag: userTags } = queryArgs;
      const savedAdsData = (await readFile('src/db/data.json', 'utf8')) || '';

      /* 
        If the data source is empty this means that
        API is unavailable for proper usage at the moment
      */
      if (!savedAdsData) {
        console.error(
          'The data.json file is empty, please fill it with Ads data before any further API usage',
        );
        throw HttpStatus.SERVICE_UNAVAILABLE;
      }

      /* 
        Select ads from data.json file that correspond to
        user's geo location, browser and OS.
      */
      const servableAdsData = this.defineServableAdsData({
        savedAdsData,
        headers,
        userLat,
        userLong,
      });

      if (!servableAdsData.length) {
        console.warn('There are no viable ads for the requested targeting.');
        throw HttpStatus.NOT_FOUND;
      }

      // Define priority for the servable ads
      const prioritizedAdsData = this.definePrioritizedAdsData(
        servableAdsData,
        userTags,
      );

      // Sort prioritized ads data and return
      return this.sortAdsData(prioritizedAdsData);
    } catch (error) {
      if (error === HttpStatus.SERVICE_UNAVAILABLE) {
        throw new ServiceUnavailableException();
      } else if (error === HttpStatus.NOT_FOUND) {
        throw new NotFoundException();
      }
      throw new Error(error.message);
    }
  }

  private defineServableAdsData = (params: {
    savedAdsData: string;
    headers: string;
    userLat: number;
    userLong: number;
  }): CreateAdDto[] => {
    const { savedAdsData, headers, userLat, userLong } = params;
    let parsedAdsData: CreateAdDto[];

    try {
      parsedAdsData = JSON.parse(savedAdsData);
    } catch (error) {
      throw new Error(error.message);
    }

    const uaParser = new UAParser(headers);

    return parsedAdsData.filter((ad: CreateAdDto) => {
      const {
        targeting: {
          location: { lat, long, radius },
        },
      } = ad;

      // Check if user is withing acceptable targeting radius
      const isWithingRadius = isPointWithinRadius(
        {
          latitude: userLat,
          longitude: userLong,
        },
        {
          latitude: lat,
          longitude: long,
        },
        radius, // Value in meters
      );

      // Check user's browser and OS
      const isValidBrowser =
        BROWSERS[uaParser.getBrowser().name] >= 0 ? true : false;
      const isValidOS = OS[uaParser.getOS().name] >= 0 ? true : false;

      if (isWithingRadius && isValidBrowser && isValidOS) {
        return ad;
      }
    });
  };

  private definePrioritizedAdsData = (
    servableAdsData: CreateAdDto[],
    userTags: string[],
  ): PrioritizedAdsData[] => {
    return servableAdsData.map((ad: CreateAdDto) => {
      const priority = intersection(ad.targeting.tags, userTags).length;
      return { priority, ad };
    });
  };

  private sortAdsData = (
    prioritizedAdsData: PrioritizedAdsData[],
  ): CreateAdDto[] => {
    const sortAdsData = orderBy(prioritizedAdsData, ['priority'], ['desc']);
    return sortAdsData.map((data) => {
      return data.ad;
    });
  };
}
