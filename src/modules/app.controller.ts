import {
  Body,
  Controller,
  Get,
  Headers,
  Post,
  Query,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';

import { AppService } from './app.service';
import { QueryParamsDto } from '../dto/query-params.dto';
import { CreateAdDto } from '../dto/create-ad.dto';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Post('ads')
  @UsePipes(ValidationPipe)
  async postAd(@Body() ad: CreateAdDto): Promise<void> {
    return this.appService.saveAdd(ad);
  }

  @Get('ads')
  async getAds(
    @Query() queryArgs: QueryParamsDto,
    @Headers() headers: { [x: string]: any },
  ): Promise<CreateAdDto[]> {
    return this.appService.getAds(queryArgs, headers['user-agent']);
  }
}
