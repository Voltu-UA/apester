import { IsString } from 'class-validator';

export class QueryParamsDto {
  @IsString()
  lat: string;
  @IsString()
  long: string;
  @IsString({ each: true })
  tag: string[];
}
