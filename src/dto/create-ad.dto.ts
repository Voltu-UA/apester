import { Type } from 'class-transformer';
import {
  IsLongitude,
  IsNumber,
  IsString,
  IsUrl,
  ValidateNested,
} from 'class-validator';

class Coordinates {
  @IsNumber()
  lat: number;
  @IsLongitude()
  long: number;
  @IsNumber()
  radius: number;
}

class AdTargeting {
  @Type(() => Coordinates)
  @ValidateNested({ each: true })
  location: Coordinates;
  @IsString({ each: true })
  operatingSystems: string[];
  @IsString({ each: true })
  browsers: string[];
  @IsString({ each: true })
  tags: string[];
}

export class CreateAdDto {
  @IsString()
  id: string;
  @IsString()
  description: string;
  @IsUrl()
  imageUrl: string;
  @Type(() => AdTargeting)
  @ValidateNested()
  targeting: AdTargeting;
}
