import { CreateAdDto } from 'dto/create-ad.dto';

export interface PrioritizedAdsData {
  priority: number;
  ad: CreateAdDto;
}
