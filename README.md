## Description

Simple app for ads targeting

## Installation

```bash
# Make sure that you have installed Node.js (v14+) and NPM (v6+) on your system.

# Clone the repo or download and unzip it to any directory on your machine with appropriate archive manager.

# run this command from the root project folder:
$ npm install
```

## Running the app

```bash
# standart app launching mode
$ npm run start

# development with watch mode
$ npm run start:dev

# development with debugging mode
$ npm run start:debug
```

## Testing the app's GET /ads route

```bash
# unit tests
$ npm run test
```

## Common recommendations

```bash
# Preferred way for app testing is to use any browser you like instead of Postman
# because with such approach you will have correct headers from which application
# fetches data about user's browser and OS.

# There is also a Postman collection in the root project directory that can help you to test
# application after installation as well.
# For the GET /ads route there is a hardcoded [user-agent] headers property, because Postman
# by itself sends headers data differently then regular browsers do.
```
